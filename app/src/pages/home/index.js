import React from 'react';

import Header from '../../components/macro/header';
import ControlBar from '../../components/macro/controlBar';
import TagList from '../../components/macro/tagList';
import AnotationList from '../../components/macro/anotationList';


import './style.css';

export default function Home() {
    return (
        <div className="PageRoot White">
            <Header/>
            <ControlBar/>
            <AnotationList>
                <TagList/>
            </AnotationList>
        </div>
    )
}