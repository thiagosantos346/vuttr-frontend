## Available Packages

### `components`

    Here we have all components used to construct all pages in de project.

#### Into this package we have:

- *micro* basically are stateless components.

- *macro* maybe not have state, but basically are components constructed with more of one component, in general *micro* components.

### `icons`

- All icons of project, builded with *SVG* and *CSS*.

### `pages`
 
- All routes pages of project stayed into of this package.

### `styles`

- Here we have all global styles, include typography and color reference.
