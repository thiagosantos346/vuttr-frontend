import React from 'react';

import './styles.css';

function inputText({label, name, placeholder}) {

    const inputId = `${name}input-text`

    return (
        <label className="input-text-label">
            {label}
            <input
                className="input-text"
                type="text"
                name={name}
                id={inputId}
                placeholder={placeholder}/>
        </label>
    );
}

export default inputText;