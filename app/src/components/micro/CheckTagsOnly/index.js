import React from 'react';

import './styles.css'

export default function CheckTagsOnly() {
  return (
    <div>
      <label className="checkbox" hmtlfor="myCheckBoxId">
        <input className="checkbox-input" type="checkbox" name="myCheckBoxName" id="myCheckBoxId"/>
        <div className="checkbox-box" ></div>
        <div className="checkbox-text" >Search tag's only</div>
      </label>
    </div>
  );
}
