import React from 'react';

import './styles.css'

function Modal({status, children}) {

    const className = `modal-${status}`;

    return (
        <div className={className}>
            <div className="modal-backdrop"></div>
            <div className="modal-content">
                {children}
            </div>
        </div>
    )
}
export default Modal