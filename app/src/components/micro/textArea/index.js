import React from 'react';

import './styles.css';

function textArea({
    name,
    label,
    placeholder,
    cols,
    rows,
    maxlength}) {

    const textId = `${name}-text-area`

    return(
        <label className="text-area-label">
            {label}
            <textarea
                className="text-area"
                name={label} 
                id={textId}
                cols={cols}
                rows={rows}
                placeholder={placeholder}
                maxlength={maxlength}
            />
        </label>
    );
}

export default textArea;