import React from 'react';
import './style.css';


export default function addButton({objectId, observers,  message }) {
    
    const notifyAll = function(command) {
        let numberOfObservers = observers.length;

        for( let i = 0; i < numberOfObservers; i++){
            observers[i](command)
        }
    }


    const handleClick = (objectId, message ) => {
        
        const command = {
            'objetctId' : objectId,
            'value' : message,
        };

        notifyAll(command);
    }

    return (
        <button className="buttom-add" onClick={ () => handleClick(objectId, message) }>
            <p>Add</p>
        </button>
    )
}
