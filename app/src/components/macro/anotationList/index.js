import React from 'react';
import AnotationItem from './anotationItem';
import './style.css';

export default function AnotationList(props) {
    return (
        <ul className="Anotation-List">
          <AnotationItem
               id="1"
               title="Notion"
               body="All in one tool to organize teams and ideias. Write, plan, collaborate, and get organized."
               children={props.children}
          />
          <AnotationItem
               id="2"
               title="Notion"
               body="All in one tool to organize teams and ideias. Write, plan, collaborate, and get organized."
               children={props.children}
          /> 
          <AnotationItem
               id="3"
               title="Notion"
               body="All in one tool to organize teams and ideias. Write, plan, collaborate, and get organized."
               children={props.children}
          /> 
        </ul>
    )
}
