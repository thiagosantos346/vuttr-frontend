import React from 'react';
import Delete from '../../../micro/buttom/delete'
import './style.css';

export default function AnotationItem(props) {
    return (
        <li className="ModalHuge" id={props.id}>
            
            <section className="ModalHuge-Header">
                <h1>{props.title}</h1>
                <Delete className="push-right"/>
            </section>
            <p>{props.body}</p>
            <div>{props.children}</div>

        </li>
    )
}
