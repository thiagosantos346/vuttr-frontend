import React, { Component } from 'react';

import InputText from '../../micro/inputText';
import TextArea from '../../micro/textArea';
import AddButtom from '../../micro/buttom/add';

import './styles.css'

export default class Form extends Component {

    render() {
        return (
            <section className="Form-Container">
                <header className="Form-Header">
                    <div className="Plus-2px IconSmall" />
                    <h2 className="Form-Header-Title"> addTool </h2>
                </header>
                <form>
                    <section className="Form-Content">
                        <InputText
                            label="Tool Name"
                            name="tool-name"
                            placeholder="hotel"
                        />
                        <InputText
                            label="Tool Link"
                            name="tool-link"
                            placeholder="https://example.com"
                        />
                        <TextArea 
                            label="Tool description"
                            name="tool-description"
                            placeholder="Describe why is the use of that tool..."
                            row="4"
                            cols="50"
                        />
                        <InputText
                            label="Tool tags"
                            name="tool-tags"
                            placeholder="#tool #control #version"
                        />
                    </section>
                </form>
            </section>
        )
    }
}
