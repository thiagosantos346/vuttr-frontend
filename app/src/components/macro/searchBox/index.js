import React from 'react';

import './styles.css';
/**
 * Realmente é necessário esse componente? Pense sem subistiuir ele por um inputText
 */
export default function searchBox() {
  return (
    <div>
      <label htmlFor="mySearchBox" >
        <input id="mySearchBox" className="SearchBox" type="text" placeholder="Search"/>
      </label>
    </div>
  );
}
