import React from 'react';
import DeleteButton from  '../../../micro/buttom/delete';

import './style.css';

export default function TagItem(props) {
    return (
        <li className="TagItem" id={props.id}>
            <article className="TagLabel" >
                <p>{props.value}</p>
            </article>
            <article className="TagDelete" >
                <DeleteButton/>
            </article>
        </li>
    )
}
