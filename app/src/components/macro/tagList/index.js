import React from 'react';
import TagItem from './tagItem';
import './style.css';

export default function TagList() {
    return (
        <section className="TagList">
            <ul>
                <TagItem 
                    value="organize"
                    id="1"
                />
                <TagItem 
                    value="Planning"
                    id="2"
                />
                <TagItem 
                    value="Collaboration"
                    id="3"
                /> 
            </ul> 
        </section>
    )
}
