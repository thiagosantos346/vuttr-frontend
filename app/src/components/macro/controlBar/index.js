import React, { useState } from 'react';

import AddButtom from '../../micro/buttom/add';
import SearchBox from '../searchBox';
import CheckTagsOnly from '../../micro/CheckTagsOnly';
import Modal from '../../micro/modal';

import Form from '../form';

import './styles.css';

export default function ControlBar(props) {
    
    //const actions = props.actions;
    const [status, setStatus] = useState('hide')

    const observerFunction = function(command){
        
        switch (command.value) {
            case 'showModal':
                    setStatus('show')
                break;
            case 'closeModal':
                setStatus('hide')
                break;
            default:
                    console.log(`Comando ${command.value}, não é valido.`)
                break;
        }
    }

    return (
        <div className="ControlBar">
            <div className="ControlBar-item" >
                <div className="ControlBar-item-Search">
                    <SearchBox className="ControlBar-item-Search-box" />
                    <CheckTagsOnly/>
                </div>
            </div>
            <div className="ControlBar-item" >   
                <AddButtom
                    objectId='btnShowModal'
                    message='showModal'
                    observers={ [observerFunction] }
                />
            </div>
            <Modal status={status} >
                <Form/>
                 <AddButtom
                    objectId='btnCloseModal'
                    message='closeModal'
                    observers={ [observerFunction] }
                />
            </Modal>
        </div>
    )
}
