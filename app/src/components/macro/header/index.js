import React from 'react'
import './style.css'

export default function Header() {
    return (
    <header className="header">
        <h1>VUTTR</h1>
        <h3>Very Useful Tools to Remember</h3>
    </header>
    )
}
